package com.opc.tourguide.controller;


import com.opc.tourguide.bean.*;
import com.opc.tourguide.exception.EntityNotFoundException;
import com.opc.tourguide.proxy.IGpsProxy;
import com.opc.tourguide.proxy.IRewardProxy;
import com.opc.tourguide.proxy.IUserProxy;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.constraints.Range;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@RestController
@Slf4j
@Validated
public class TourGuideController {

    /**
     * The User proxy.
     */
    @Autowired
    IUserProxy iUserProxy;

    /**
     * The Attraction proxy.
     */
    @Autowired
    IGpsProxy iGpsProxy;

    /**
     * The Reward proxy.
     */
    @Autowired
    IRewardProxy iRewardProxy;

    /**
     * Index string.
     *
     * @return the string
     */
    @ApiOperation(
            value = "Displays a welcome message.")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "A string.")
            }
    )
    @GetMapping("/")
    public String index() {
        return "Greetings from TourGuide!";
    }

    /**
     * Gets location.
     *
     * @param userId the user id
     * @return the location
     * @throws EntityNotFoundException if the user is not found
     */
    @ApiOperation(
            value = "Gets rewards for a user.", notes = "${notes.entityNotFoundExceptionNote}")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "The user's location"),
                    @ApiResponse(code = 400, message = "If the id is not a valid uuid or the user does not exist")
            }
    )
    @GetMapping("/getLocation")
    public LocationBean getLocation(@RequestParam UUID userId) throws EntityNotFoundException {
        VisitedLocationBean visitedLocation = iGpsProxy.getUserLocation(userId);
        return visitedLocation.location;
    }

    /**
     * Gets nearby attractions.
     *
     * @param userId the user id
     * @return the nearby attractions
     * @throws EntityNotFoundException if the user is not found
     */
    @ApiOperation(
            value = "Gets nearby attractions for a user.", notes = "${notes.entityNotFoundExceptionNote}")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "A list of nearby attractions"),
                    @ApiResponse(code = 400, message = "If the id is not a valid uuid or the user does not exist")
            }
    )
    @GetMapping("/getNearbyAttractions")
    public List<NearByAttractionBean> getNearbyAttractions(@RequestParam UUID userId) throws EntityNotFoundException {
        VisitedLocationBean visitedLocation = iGpsProxy.getUserLocation(userId);
        return iGpsProxy.getNearByAttractions(visitedLocation);
    }

    /**
     * Gets rewards.
     *
     * @param userId the user id
     * @return the rewards
     */
    @ApiOperation(
            value = "Gets rewards for a user.", notes = "${notes.entityNotFoundExceptionNote}")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "A list of rewards"),
                    @ApiResponse(code = 400, message = "If the id is not a valid uuid or the user does not exist")
            }
    )
    @GetMapping("/getRewards")
    public List<UserRewardBean> getRewards(@RequestParam UUID userId) {
        return iUserProxy.getUserRewards(userId);
    }

    /**
     * Gets all current locations.
     *
     * @return the all current locations
     */
    @ApiOperation(
            value = "Gets all users last location.", notes = "${notes.entityNotFoundExceptionNote}")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "A map of users id and their last visited location"),
                    @ApiResponse(code = 400, message = "If the id is not a valid uuid")
            }
    )
    @GetMapping("/getAllCurrentLocations")
    public Map<String, Map<String, Double>> getAllCurrentLocations() {
        return iUserProxy.getAllUsersLastLocation();
    }

    /**
     * Gets trip deals.
     *
     * @param userId the user id
     * @return the trip deals
     * @throws EntityNotFoundException the entity not found exception
     */
    @ApiOperation(
            value = "Gets trip deals for the user.", notes = "${notes.entityNotFoundExceptionNote}")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "The trip providers list"),
                    @ApiResponse(code = 400, message = "If the id is not a valid uuid")
            }
    )
    @GetMapping("/getTripDeals")
    public List<ProviderBean> getTripDeals(@RequestParam UUID userId) throws EntityNotFoundException {
        List<ProviderBean> providers = iRewardProxy.getProviders(userId);
        log.info(providers.toString());
        return providers;
    }

    /**
     * Track user location visited location.
     *
     * @param userBean the user bean
     * @return the visited location
     * @throws EntityNotFoundException if the user is not found
     */
    @ApiOperation(
            value = "Tracks a user's location and initiate the reward calculation process.", notes = "${notes" +
                                                                                                     ".entityNotFoundExceptionNote}")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "The visited location"),
                    @ApiResponse(code = 400, message = "If the provided user is not a valid user")
            }
    )
    @PostMapping("/trackUserLocation")
    public VisitedLocationBean trackUserLocation(
            @Valid @RequestBody @NotNull(message = "user must not be null") UserBean userBean) throws
            EntityNotFoundException {
        return iGpsProxy.trackUserLocation(userBean.getUserId());
    }

    /**
     * Track user location asynchronously for a list of users.
     *
     * @param usersCount the users list
     * @return the visited location
     * @throws EntityNotFoundException if the user is not found
     */
    @ApiOperation(
            value = "Track user's location asynchronously.", notes = "${notes" +
                                                                     ".entityNotFoundExceptionNote}")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "The visited location"),
                    @ApiResponse(code = 400, message = "If the provided users count is not in the allowed range")
            }
    )
    @GetMapping("/trackUserLocationAsync/{usersCount}")
    public List<VisitedLocationBean> trackUserLocationAsync(
            @PathVariable("usersCount") @Range(min = 1, max = 100_000) int usersCount) throws
            EntityNotFoundException {
        return iGpsProxy.trackUserLocationAsync(usersCount);
    }

    /**
     * Stops the tracking process.
     */
    @ApiOperation(
            value = "Gently stops the current tracking process.")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "No content."),
            }
    )
    @PostMapping("/stopTracking")
    public void stopTracking() {
        iGpsProxy.stopTracking();
    }

}