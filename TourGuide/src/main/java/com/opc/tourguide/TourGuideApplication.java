package com.opc.tourguide;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients("com.opc.tourguide")
public class TourGuideApplication {

    public static void main(String[] args) {
        SpringApplication.run(TourGuideApplication.class, args);
    }

}