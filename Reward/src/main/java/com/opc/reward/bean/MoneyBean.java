package com.opc.reward.bean;

import org.javamoney.moneta.spi.MoneyUtils;

import javax.money.CurrencyUnit;
import java.math.BigDecimal;


public class MoneyBean {

    /**
     * Instantiates a new Money bean.
     *
     * @param number       the number
     * @param currencyCode the currency code
     */
    public MoneyBean(BigDecimal number, CurrencyUnit currencyCode) {
        this.monetaryContext = currencyCode;
        this.number          = number;
    }

    /**
     * Of money bean.
     *
     * @param number       the number
     * @param currencyCode the currency code
     * @return the money bean
     */
    public static MoneyBean of(Number number, CurrencyUnit currencyCode) {
        return new MoneyBean(MoneyUtils.getBigDecimal(number), currencyCode);
    }

    private final CurrencyUnit monetaryContext;

    private final BigDecimal number;

    @Override public String toString() {
        return "MoneyBean{" +
               "monetaryContext=" + monetaryContext +
               ", number=" + number +
               '}';
    }
}