package com.opc.reward.bean;

public class NearByAttractionBean {

    private String name;

    private gpsUtil.location.Location attractionLocation;

    private gpsUtil.location.Location userLocation;

    private double distanceInMiles;

    private int rewardPoints;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public gpsUtil.location.Location getAttractionLocation() {
        return attractionLocation;
    }

    public void setAttractionLocation(gpsUtil.location.Location attractionLocation) {
        this.attractionLocation = attractionLocation;
    }

    public gpsUtil.location.Location getUserLocation() {
        return userLocation;
    }

    public void setUserLocation(gpsUtil.location.Location userLocation) {
        this.userLocation = userLocation;
    }

    public double getDistanceInMiles() {
        return distanceInMiles;
    }

    public void setDistanceInMiles(double distanceInMiles) {
        this.distanceInMiles = distanceInMiles;
    }

    public int getRewardPoints() {
        return rewardPoints;
    }

    public void setRewardPoints(int rewardPoints) {
        this.rewardPoints = rewardPoints;
    }
}