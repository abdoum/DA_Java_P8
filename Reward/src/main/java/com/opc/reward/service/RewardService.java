package com.opc.reward.service;

import com.opc.reward.bean.LocationBean;
import com.opc.reward.bean.UserBean;
import com.opc.reward.bean.VisitedLocationBean;
import com.opc.reward.exception.EntityNotFoundException;
import com.opc.reward.model.UserReward;
import com.opc.reward.proxy.IUserProxy;
import gpsUtil.GpsUtil;
import gpsUtil.location.Attraction;
import gpsUtil.location.Location;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import rewardCentral.RewardCentral;
import tripPricer.Provider;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Service
@Slf4j
public class RewardService implements IRewardService {

    @Value("${statute.miles.nauticalMiles}")
    private double STATUTE_MILES_PER_NAUTICAL_MILE = 1.15077945;

    private int ATTRACTION_PROXIMITY_RANGE = Integer.MAX_VALUE;

    @Autowired
    GpsUtil gpsUtil;

    @Autowired
    RewardCentral rewardsCentral;

    @Autowired
    IUserProxy iUserProxy;

    @Autowired
    ITripPricerService iTripPricerService;

    /**
     * Gets providers.
     *
     * @param userId the user id
     * @return A list of providers
     * @throws EntityNotFoundException if the user is not found
     */
    @Override public List<Provider> getProviders(UUID userId) throws EntityNotFoundException {
        UserBean user = getUserById(userId);
        return iTripPricerService.getProviders(user);
    }

    /**
     * Calculates rewards for a yser.
     *
     * @param userBean the user
     */
    @Override public void calculateRewards(UserBean userBean) {

        List<VisitedLocationBean> userLocations = (List) ((ArrayList) userBean.getVisitedLocations()).clone();
        List<Attraction> attractions = gpsUtil.getAttractions();
        ExecutorService executor = Executors.newFixedThreadPool(4);
        List<Callable<String>> callableTasks = new ArrayList<>();
        for (VisitedLocationBean visitedLocation : userLocations) {
            for (Attraction attraction : attractions) {
                Callable<String> calculate = () -> {
                    boolean userHasNoRewards = isUserHasNoRewards(userBean, attraction);
                    if (userHasNoRewards) {
                        boolean isAttractionNear = isWithinAttractionProximity(attraction, visitedLocation.location);
                        if (isAttractionNear) {
                            UserReward newAttraction = createUserReward(userBean, visitedLocation, attraction);
                            userBean.addUserReward(newAttraction);
                        }
                    }
                    return "reward calculation task";
                };
                callableTasks.add(calculate);
            }
        }
        try {
            executor.invokeAll(callableTasks);
        }
        catch (InterruptedException e) {
            log.error("error when calculating user location", e);
            Thread.currentThread().interrupt();
            executor.shutdown();
        }
        executor.shutdown();
    }

    /**
     * Calculates rewards asynchronously for multiple users.
     *
     * @param userBeanList the users list
     */
    @Override public void calculateRewardsAsync(List<UserBean> userBeanList) {

        ExecutorService executor = Executors.newFixedThreadPool(500);
        List<Callable<String>> callableTasks = new ArrayList<>();
        userBeanList.forEach(userBean -> {
            Callable<String> calculate = () -> {
                calculateRewards(userBean);
                return "reward calculation task";
            };
            callableTasks.add(calculate);
        });
        try {
            executor.invokeAll(callableTasks);
        }
        catch (InterruptedException e) {
            e.printStackTrace();
            Thread.currentThread().interrupt();
            executor.shutdown();
        }
        executor.shutdown();
    }

    /**
     * Gets an attraction's reward points.
     *
     * @param attractionId the attraction id
     * @param userId       the user id
     * @return the attraction's reward points
     * @throws EntityNotFoundException the entity not found exception if no user is found with the provided id
     */
    @Override public int getAttractionRewardPoints(UUID attractionId, UUID userId) throws EntityNotFoundException {
        if (iUserProxy.getUser(userId) == null) {
            throw new EntityNotFoundException("no user with id : " + userId + " could be found");
        }
        return rewardsCentral.getAttractionRewardPoints(attractionId, userId);
    }

    /**
     * Checks if an attraction is within proximity range.
     *
     * @param attraction the attraction
     * @param location   the location
     * @return a boolean, true if the attraction is within proximity range, false otherwise
     */
    @Override public boolean isWithinAttractionProximity(Attraction attraction, LocationBean location) {
        return getDistance(attraction, location) < ATTRACTION_PROXIMITY_RANGE;
    }

    /**
     * Gets the distance between 2 locations.
     *
     * @param loc1 the first location
     * @param loc2 the second location
     * @return the distance
     */
    @Override public double getDistance(Location loc1, LocationBean loc2) {
        double lat1 = Math.toRadians(loc1.latitude);
        double lon1 = Math.toRadians(loc1.longitude);
        double lat2 = Math.toRadians(loc2.latitude);
        double lon2 = Math.toRadians(loc2.longitude);

        double angle = Math.acos(Math.sin(lat1) * Math.sin(lat2)
                                 + Math.cos(lat1) * Math.cos(lat2) * Math.cos(lon1 - lon2));

        double nauticalMiles = 60 * Math.toDegrees(angle);
        return STATUTE_MILES_PER_NAUTICAL_MILE * nauticalMiles;
    }

    @Override public void calculateRewardsById(UUID userId) throws EntityNotFoundException {
        UserBean user = getUserById(userId);
        calculateRewards(user);
    }

    /**
     * Retreive a user by its id
     *
     * @param userId the user id
     * @return the found user
     * @throws EntityNotFoundException if no user is found with the given id
     */
    private UserBean getUserById(UUID userId) throws EntityNotFoundException {
        UserBean userBean = iUserProxy.getUser(userId);
        if (userBean == null) {
            throw new EntityNotFoundException("no user with id : " + userId + " could be found");
        }
        return userBean;
    }
}