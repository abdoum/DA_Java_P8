package com.opc.reward.service;

import com.opc.reward.bean.UserBean;
import com.opc.reward.model.UserReward;
import org.springframework.stereotype.Service;
import tripPricer.Provider;

import java.util.List;


@Service
public class TripPricerService implements ITripPricerService {

    private static final String TRIP_PRICER_API_KEY = "test-server-api-key";

    private final TripPricerImpl tripPricerImpl = new TripPricerImpl();

    /**
     * Gets a list of providers for a user.
     *
     * @param userBean the user
     * @return the list of providers
     */
    @Override public List<Provider> getProviders(UserBean userBean) {
        int cumulativeRewardPoints = userBean.getUserRewards()
                                             .parallelStream()
                                             .mapToInt(UserReward::getRewardPoints)
                                             .sum();
        return tripPricerImpl.getPrice(TRIP_PRICER_API_KEY, userBean.getUserId(),
                userBean.getUserPreferences().getNumberOfAdults(),
                userBean.getUserPreferences().getNumberOfChildren(),
                userBean.getUserPreferences().getTripDuration(),
                cumulativeRewardPoints);
    }

}