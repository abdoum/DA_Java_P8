package com.opc.reward.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.opc.reward.bean.LocationBean;
import com.opc.reward.bean.UserBean;
import com.opc.reward.bean.VisitedLocationBean;
import com.opc.reward.proxy.IUserProxy;
import com.opc.reward.service.IRewardService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class RewardControllerTest {

    @MockBean
    IRewardService iRewardService;

    @MockBean
    IUserProxy IUserProxy;

    @Autowired
    MockMvc mockMvc;

    @Autowired
    private ObjectMapper mapper;

    @BeforeEach
    void setUp() {
        mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
    }

    @Test
    void testCalculateRewards_shouldReturnHttpStatus200() throws Exception {
        // Arrange
        UUID userId = UUID.fromString("5392d8ee-2390-4b81-82ee-9e0d44c0ca5d");

        UserBean user = new UserBean(userId, "weekend", "89263478293", "nifr@kjfbrn.fr");
        when(IUserProxy.getUser(userId)).thenReturn(user);
        // Act and Assert
        mockMvc.perform(get("/rewards/" + userId))
               .andExpect(status().is(200));
    }

    @Test
    void testGetProviders_shouldReturnAListOfProviders_forAValidUserId() throws Exception {
        // Arrange
        UUID userId = UUID.fromString("5392d8ee-2390-4b81-82ee-9e0d44c0ca5d");
        UserBean user = new UserBean(userId, "weekend", "89263478293", "nifr@kjfbrn.fr");
        when(IUserProxy.getUser(userId)).thenReturn(user);
        // Act and Assert
        mockMvc.perform(get("/rewards/providers/" + userId))
               .andExpect(status().isOk())
               .andDo(print());
    }

    @Test
    void testCalculateRewardsAsync_shouldReturnAHttpStatus200_forAValidUuid() throws Exception {
        // Arrange
        UUID userId = UUID.fromString("5392d8ee-2390-4b81-82ee-9e0d44c0ca5d");
        when(iRewardService.getAttractionRewardPoints((UUID) any(), (UUID) any())).thenReturn(10);
        // Act and Assert
        mockMvc.perform(get("/rewards/points/{attractionId}/{userId}", UUID.randomUUID(), userId))
               .andExpect(status().is(200)).andExpect(content().string("10"));
    }

    @Test
    void testGetAttractionRewardPoints_shouldReturnHttpStatus200_forAValidUserList() throws Exception {
        // Arrange
        UUID userId = UUID.fromString("5392d8ee-2390-4b81-82ee-9e0d44c0ca5d");
        UserBean user = new UserBean(userId, "weekend", "89263478293", "nifr@kjfbrn.fr");
        user.addToVisitedLocations(new VisitedLocationBean(userId, new LocationBean(10.0, 10.0), new Date(1L)));
        List<UserBean> userBeanList = Collections.singletonList(user);
        doNothing().when(iRewardService).calculateRewardsAsync((List<UserBean>) userBeanList);
        // Act and Assert
        mockMvc.perform(post("/rewards/async")
                       .contentType(MediaType.APPLICATION_JSON)
                       .content(mapper.writeValueAsString(userBeanList)))
               .andExpect(status().is(200));
    }
}