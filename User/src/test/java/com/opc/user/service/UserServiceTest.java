package com.opc.user.service;

import com.opc.user.exception.EntityNotFoundException;
import com.opc.user.helper.InternalTestHelper;
import com.opc.user.model.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ContextConfiguration(classes = {UserService.class})
@ExtendWith(SpringExtension.class)
class UserServiceTest {

    @Autowired
    private IUserService iUserService;

    @MockBean
    private InternalTestHelper internalTestHelper;

    @MockBean
    Logger logger;

    @Test
    void testGetUserRewards_shouldReturnAnEmptyList_forAUserWithNoRewards() {
        // Arrange, Act and Assert
        assertTrue(
                this.iUserService.getUserRewards(new User(UUID.randomUUID(), "janedoe", "4105551212", "42 Main St"))
                                 .isEmpty());
    }

    @Test
    void testGetUserRewards_shouldReturnAListOfUserRewards_forAValidUser() {
        // Arrange
        User user = new User(UUID.randomUUID(), "janedoe", "4105551212", "42 Main St");
        UUID userId = UUID.randomUUID();
        Location location = new Location(10.0, 10.0);
        VisitedLocation visitedLocation = new VisitedLocation(userId, location, new Date(1L));
        user.addUserReward(
                new UserReward(visitedLocation, new Attraction("Attraction Name", "Oxford", "MD", 10.0, 10.0)));

        // Act and Assert
        assertEquals(1, this.iUserService.getUserRewards(user).size());
    }

    @Test
    void testGetUserById_shouldThrowAEntityNotFoundException_forANonExistingUserId() throws EntityNotFoundException {
        // Arrange
        UUID userId = UUID.randomUUID();
        // Act and Assert
        EntityNotFoundException entityNotFoundException = assertThrows(EntityNotFoundException.class,
                () -> this.iUserService.getUserById(userId));
        assertEquals("User with id " + userId + " could not be found", entityNotFoundException.getMessage());
    }

    @Test
    void testInitializeInternalUsers_shouldReturnAListOfUsers() {
        // Arrange
        when(internalTestHelper.getUsersCount()).thenReturn(3);
        when(logger.isDebugEnabled()).thenReturn(true);
        // Act and Assert
        Map<UUID, User> users = iUserService.initializeInternalUsers();
        Map.Entry<UUID, User> entry = users.entrySet().iterator().next();
        User firstUser = entry.getValue();

        assertTrue(users.size() > 1);
        assertNotNull(firstUser);
    }

    @Test
    void testGetAllUsersLastLocation_shouldReturnAMapOfUsersIdAndLocation_forExistingUsers() {
        when(internalTestHelper.getUsersCount()).thenReturn(3);
        Map<String, List<Location>> usersLocations = iUserService.getAllUsersLocations();
        assertTrue(usersLocations.size() > 1);
    }

    @Test
    void testGetUserById_shouldReturnTheFoundUser_forAValidUserId() {
        when(internalTestHelper.getUsersCount()).thenReturn(3);
        Map<UUID, User> users = iUserService.initializeInternalUsers();
        Map.Entry<UUID, User> entry = users.entrySet().iterator().next();
        UUID firstUserId = entry.getKey();
        User firstUser = entry.getValue();
        assertNotNull(firstUser);
        assertNotNull(firstUserId);
    }
}