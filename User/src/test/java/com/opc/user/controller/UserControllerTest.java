package com.opc.user.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.opc.user.model.*;
import com.opc.user.service.IUserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class UserControllerTest {

    @MockBean
    IUserService iUserService;

    @Autowired
    MockMvc mockMvc;

    @Autowired
    private ObjectMapper mapper;

    @BeforeEach
    void setUp() {
        mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
    }

    @Test
    void testGetAllUsers_shouldReturnAListOfUsers() throws Exception {
        // Arrange
        when(this.iUserService.initializeInternalUsers()).thenReturn(new HashMap<UUID, User>(1));
        // Act and Assert
        mockMvc.perform(get("/users"))
               .andExpect(status().isOk())
               .andExpect(content().contentType("application/json"));
    }

    @Test
    void testGetUserById_shouldReturnAUserAndAnHttp200_forAnExistingUser() throws Exception {
        // Arrange
        UUID userId = UUID.fromString("5622b706-26a9-4f40-b167-fd6f78a989c9");
        when(this.iUserService.initializeInternalUsers()).thenReturn(new HashMap<UUID, User>(0));
        // Act and Assert
        mockMvc.perform(get("/users/" + userId))
               .andExpect(status().is(200));
    }

    @Test
    void testAddUser_shouldReturnHttp200_forASuccessfullyUserAdded() throws Exception {
        User user = new User(UUID.fromString("0c9ebdd1-5809-4eda-a441-a465e4bab5f8"),
                "cure", "9387459834", "until@fol.fr");
        user.addToVisitedLocations(
                new VisitedLocation(user.getUserId(), new Location(98734.3948, 9834.340), new Date()));
        user.setUserPreferences(new UserPreferences());

        mockMvc.perform(post("/users")
                       .contentType(MediaType.APPLICATION_JSON)
                       .content(mapper.writeValueAsString(user)))
               .andExpect(status().isOk());
    }

    @Test
    void testGetAllUsersLastLocation_shouldReturnAnHttp200StatusCode() throws Exception {
        when(this.iUserService.initializeInternalUsers()).thenReturn(new HashMap<UUID, User>(0));
        mockMvc.perform(get("/users/locations"))
               .andExpect(status().isOk())
               .andExpect(content().contentType("application/json"))
               .andExpect(jsonPath("$").isMap());
    }

    @Test
    void testGetUserRewards_shouldReturnAnHttp200StatusCode_forAnExistingUser() throws Exception {
        User user = new User(UUID.fromString("0c9ebdd1-5809-4eda-a441-a465e4bab5f8"),
                "cure", "9387459834", "until@fol.fr");
        user.addToVisitedLocations(
                new VisitedLocation(user.getUserId(), new Location(98734.3948, 9834.340), new Date()));
        user.setUserPreferences(new UserPreferences());

        when(this.iUserService.initializeInternalUsers()).thenReturn(new HashMap<UUID, User>(0));
        when(this.iUserService.getUserRewards(user)).thenReturn(new ArrayList<UserReward>(0));

        mockMvc.perform(get("/users/rewards/" + user.getUserId()))
               .andExpect(status().isOk())
               .andExpect(content().contentType("application/json"));
    }

    @Test
    void testSetUserCount_shouldReturnTheNewUserCountAndAnHttp200StatusCode() throws Exception {
        mockMvc.perform(put("/users/count/1"))
               .andExpect(status().isOk())
               .andExpect(content().contentType("application/json"))
               .andExpect(content().string("1"));
    }

}