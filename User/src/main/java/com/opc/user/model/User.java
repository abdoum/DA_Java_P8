package com.opc.user.model;


import com.opc.user.bean.ProviderBean;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.*;


public class User {

    private final UUID userId;

    @NotBlank
    @Size(min= 4, max=50)
    private final String userName;

    @NotBlank
    @Size(min= 6, max=10)
    private String phoneNumber;

    @NotBlank
    @Email
    private String emailAddress;

    @DateTimeFormat
    private Date latestLocationTimestamp;

    private List<VisitedLocation> visitedLocations = new ArrayList<>();

    private List<UserReward> userRewards = new ArrayList<>();

    private UserPreferences userPreferences = new UserPreferences();

    private List<ProviderBean> tripDeals = new ArrayList<>();

    /**
     * Instantiates a new User.
     *
     * @param userId       the user id
     * @param userName     the username
     * @param phoneNumber  the phone number
     * @param emailAddress the email address
     */
    public User(UUID userId, String userName, String phoneNumber, String emailAddress) {
        this.userId       = userId;
        this.userName     = userName;
        this.phoneNumber  = phoneNumber;
        this.emailAddress = emailAddress;
    }

    /**
     * Gets user id.
     *
     * @return the user id
     */
    public UUID getUserId() {
        return userId;
    }

    /**
     * Gets username.
     *
     * @return the username
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Gets phone number.
     *
     * @return the phone number
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * Sets phone number.
     *
     * @param phoneNumber the phone number
     */
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /**
     * Gets email address.
     *
     * @return the email address
     */
    public String getEmailAddress() {
        return emailAddress;
    }

    /**
     * Sets email address.
     *
     * @param emailAddress the email address
     */
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    /**
     * Gets latest location timestamp.
     *
     * @return the latest location timestamp
     */
    public Date getLatestLocationTimestamp() {
        return latestLocationTimestamp;
    }

    /**
     * Sets latest location timestamp.
     *
     * @param latestLocationTimestamp the latest location timestamp
     */
    public void setLatestLocationTimestamp(Date latestLocationTimestamp) {
        this.latestLocationTimestamp = latestLocationTimestamp;
    }

    /**
     * Add to visited locations.
     *
     * @param visitedLocation the visited location
     */
    public void addToVisitedLocations(VisitedLocation visitedLocation) {
        visitedLocations.add(visitedLocation);
    }

    /**
     * Gets visited locations.
     *
     * @return the visited locations
     */
    public List<VisitedLocation> getVisitedLocations() {
        return visitedLocations;
    }

    /**
     * Clear visited locations.
     */
    public void clearVisitedLocations() {
        visitedLocations.clear();
    }

    /**
     * Add user reward.
     *
     * @param userReward the user reward
     */
    public void addUserReward(UserReward userReward) {
        if (userRewards.stream()
                       .noneMatch(reward -> reward.attraction.attractionName.equals(
                               userReward.attraction.attractionName))) {
            userRewards.add(userReward);
        }
    }

    /**
     * Gets user rewards.
     *
     * @return the user rewards
     */
    public List<UserReward> getUserRewards() {
        return userRewards;
    }

    /**
     * Gets user preferences.
     *
     * @return the user preferences
     */
    public UserPreferences getUserPreferences() {
        return userPreferences;
    }

    /**
     * Sets user preferences.
     *
     * @param userPreferences the user preferences
     */
    public void setUserPreferences(UserPreferences userPreferences) {
        this.userPreferences = userPreferences;
    }

    /**
     * Gets trip deals.
     *
     * @return the trip deals
     */
    public List<ProviderBean> getTripDeals() {
        return tripDeals;
    }

    /**
     * Sets trip deals.
     *
     * @param tripDeals the trip deals
     */
    public void setTripDeals(List<ProviderBean> tripDeals) {
        this.tripDeals = tripDeals;
    }

    /**
     * Gets location to string.
     *
     * @param location the location
     * @return the location to string
     */
    public Map<String, Double> getLocationToString(Location location) {
        Map<String, Double> locationMap = new HashMap<>();
        locationMap.put("latitude", location.latitude);
        locationMap.put("longitude", location.longitude);
        return locationMap;
    }

}