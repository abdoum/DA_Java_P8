package com.opc.user.model;


import javax.money.CurrencyUnit;
import javax.money.Monetary;

public class UserPreferences {

    /**
     * The Currency.
     */
    private final CurrencyUnit currency = Monetary.getCurrency("USD");

    /**
     * The Attraction proximity.
     */
    private int attractionProximity = Integer.MAX_VALUE;

    /**
     * The Lower price point.
     */
    private Money lowerPricePoint = Money.of(0, currency);

    /**
     * The High price point.
     */
    private Money highPricePoint = Money.of(Integer.MAX_VALUE, currency);

    /**
     * The Trip duration.
     */
    private int tripDuration = 1;

    /**
     * The Ticket quantity.
     */
    private int ticketQuantity = 1;

    /**
     * The Number of adults.
     */
    private int numberOfAdults = 1;

    /**
     * The Number of children.
     */
    private int numberOfChildren = 0;

    /**
     * Instantiates a new User preferences.
     */
    public UserPreferences() {
    }

    /**
     * Gets the specified attraction proximity preference for the user.
     *
     * @return the attraction proximity
     */
    public int getAttractionProximity() {
        return attractionProximity;
    }

    /**
     * Sets the attraction proximity preferred value for the user.
     *
     * @param attractionProximity the attraction proximity
     */
    public void setAttractionProximity(int attractionProximity) {
        this.attractionProximity = attractionProximity;
    }

    /**
     * Gets the lower price point.
     *
     * @return the lower price point
     */
    public Money getLowerPricePoint() {
        return lowerPricePoint;
    }

    /**
     * Sets a lower price point.
     *
     * @param lowerPricePoint the lower price point
     */
    public void setLowerPricePoint(Money lowerPricePoint) {
        this.lowerPricePoint = lowerPricePoint;
    }

    /**
     * Gets the high price point.
     *
     * @return the high price point
     */
    public Money getHighPricePoint() {
        return highPricePoint;
    }

    /**
     * Sets the high price point.
     *
     * @param highPricePoint the high price point
     */
    public void setHighPricePoint(Money highPricePoint) {
        this.highPricePoint = highPricePoint;
    }

    /**
     * Gets the trip duration.
     *
     * @return the trip duration
     */
    public int getTripDuration() {
        return tripDuration;
    }

    /**
     * Sets the trip duration.
     *
     * @param tripDuration the trip duration
     */
    public void setTripDuration(int tripDuration) {
        this.tripDuration = tripDuration;
    }

    /**
     * Gets the ticket quantity.
     *
     * @return the ticket quantity
     */
    public int getTicketQuantity() {
        return ticketQuantity;
    }

    /**
     * Sets ticket quantity.
     *
     * @param ticketQuantity the ticket quantity
     */
    public void setTicketQuantity(int ticketQuantity) {
        this.ticketQuantity = ticketQuantity;
    }

    /**
     * Gets the number of adults.
     *
     * @return the number of adults
     */
    public int getNumberOfAdults() {
        return numberOfAdults;
    }

    /**
     * Sets the number of adults.
     *
     * @param numberOfAdults the number of adults
     */
    public void setNumberOfAdults(int numberOfAdults) {
        this.numberOfAdults = numberOfAdults;
    }

    /**
     * Gets the number of children.
     *
     * @return the number of children
     */
    public int getNumberOfChildren() {
        return numberOfChildren;
    }

    /**
     * Sets the number of children.
     *
     * @param numberOfChildren the number of children
     */
    public void setNumberOfChildren(int numberOfChildren) {
        this.numberOfChildren = numberOfChildren;
    }

}