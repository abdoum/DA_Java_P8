package com.opc.gps.service;

import com.opc.gps.bean.LocationBean;
import com.opc.gps.bean.UserBean;
import com.opc.gps.bean.VisitedLocationBean;
import com.opc.gps.exception.EntityNotFoundException;
import com.opc.gps.model.NearByAttraction;
import com.opc.gps.proxy.IRewardProxy;
import com.opc.gps.proxy.IUserProxy;
import gpsUtil.GpsUtil;
import gpsUtil.location.Attraction;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.ConfigDataApplicationContextInitializer;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ContextConfiguration(initializers = ConfigDataApplicationContextInitializer.class, classes = {
        GpsService.class,
        GpsUtil.class, Tracker.class
})
@ExtendWith(SpringExtension.class)
class GpsServiceTest {

    @MockBean
    private IUserProxy IUserProxy;

    @Autowired
    private IGpsService iGpsService;

    @MockBean
    private GpsUtil gpsUtil;

    @MockBean
    private IRewardProxy IRewardProxy;

    @Test
    void testGetNearbyAttractions_shouldReturnTheTopFiveNearByAttractions_forAValidUserLocation() {
        // Arrange
        List<Attraction> gpsUtilsAttractions = getAttractions();
        VisitedLocationBean visitedLocation = new VisitedLocationBean(UUID.randomUUID(), new LocationBean(10.0, 10.0),
                new Date());

        iGpsService.setAttractionProximityRange((Integer.MAX_VALUE));
        when(gpsUtil.getAttractions()).thenReturn(gpsUtilsAttractions);

        // Act and Assert
        List<NearByAttraction> attractions = iGpsService.getNearByAttractions(visitedLocation);
        assertEquals(5, attractions.size());
        verify(this.gpsUtil).getAttractions();
    }

    @Test
    void testGetNearByAttractions_shouldReturnAnEmptyListOfNearByAttractions_forNoValidUserLocation() {
        // Arrange
        iGpsService.setAttractionProximityRange(1);
        when(this.gpsUtil.getAttractions()).thenReturn(new ArrayList<>());
        UUID userId = UUID.randomUUID();
        LocationBean location = new LocationBean(10.0, 10.0);
        // Act and Assert
        assertTrue(this.iGpsService.getNearByAttractions(new VisitedLocationBean(userId, location, new Date(1L)))
                                   .isEmpty());
        verify(this.gpsUtil).getAttractions();
    }

    private List<Attraction> getAttractions() {
        List<Attraction> attractions = new ArrayList<>();
        attractions.add(new Attraction("Disneyland", "Anaheim", "CA", 33.817595D, -117.922008D));
        attractions.add(new Attraction("Jackson Hole", "Jackson Hole", "WY", 43.582767D, -110.821999D));
        attractions.add(new Attraction("Mojave National Preserve", "Kelso", "CA", 35.141689D, -115.510399D));
        attractions.add(new Attraction("Joshua Tree National Park", "Joshua Tree National Park", "CA", 33.881866D,
                -115.90065D));
        attractions.add(new Attraction("Buffalo National River", "St Joe", "AR", 35.985512D, -92.757652D));
        attractions.add(new Attraction("Hot Springs National Park", "Hot Springs", "AR", 34.52153D, -93.042267D));
        attractions.add(new Attraction("Kartchner Caverns State Park", "Benson", "AZ", 31.837551D, -110.347382D));
        attractions.add(new Attraction("Legend Valley", "Thornville", "OH", 39.937778D, -82.40667D));
        attractions.add(
                new Attraction("Flowers Bakery of London", "Flowers Bakery of London", "KY", 37.131527D, -84.07486D));
        attractions.add(new Attraction("McKinley Tower", "Anchorage", "AK", 61.218887D, -149.877502D));
        attractions.add(new Attraction("Flatiron Building", "New York City", "NY", 40.741112D, -73.989723D));
        attractions.add(new Attraction("Fallingwater", "Mill Run", "PA", 39.906113D, -79.468056D));
        attractions.add(new Attraction("Union Station", "Washington D.C.", "CA", 38.897095D, -77.006332D));
        attractions.add(new Attraction("Roger Dean Stadium", "Jupiter", "FL", 26.890959D, -80.116577D));
        attractions.add(new Attraction("Texas Memorial Stadium", "Austin", "TX", 30.283682D, -97.732536D));
        attractions.add(new Attraction("Bryant-Denny Stadium", "Tuscaloosa", "AL", 33.208973D, -87.550438D));
        attractions.add(new Attraction("Tiger Stadium", "Baton Rouge", "LA", 30.412035D, -91.183815D));
        attractions.add(new Attraction("Neyland Stadium", "Knoxville", "TN", 35.955013D, -83.925011D));
        attractions.add(new Attraction("Kyle Field", "College Station", "TX", 30.61025D, -96.339844D));
        attractions.add(new Attraction("San Diego Zoo", "San Diego", "CA", 32.735317D, -117.149048D));
        attractions.add(new Attraction("Zoo Tampa at Lowry Park", "Tampa", "FL", 28.012804D, -82.469269D));
        attractions.add(new Attraction("Franklin Park Zoo", "Boston", "MA", 42.302601D, -71.086731D));
        attractions.add(new Attraction("El Paso Zoo", "El Paso", "TX", 31.769125D, -106.44487D));
        attractions.add(new Attraction("Kansas City Zoo", "Kansas City", "MO", 39.007504D, -94.529625D));
        attractions.add(new Attraction("Bronx Zoo", "Bronx", "NY", 40.852905D, -73.872971D));
        attractions.add(new Attraction("Cinderella Castle", "Orlando", "FL", 28.419411D, -81.5812D));
        return attractions;
    }

    @Test
    void testGetUserLocation_shouldReturnAVisitedLocation_forAValidUserId() throws EntityNotFoundException {
        UserBean userBean = new UserBean(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
        when(IUserProxy.getUser(userBean.getUserId())).thenReturn(userBean);
        userBean.addToVisitedLocations(
                new VisitedLocationBean(userBean.getUserId(), new LocationBean(93847D, 987D), new Date()));
        VisitedLocationBean visitedLocation = iGpsService.getUserLocation(userBean.getUserId());
        assertNotNull(visitedLocation);
        verify(this.IRewardProxy, times(0)).calculateRewards(userBean.getUserId());
    }

    @Test
    void testTrackUserLocation_shouldReturnTheUserLocation_forAValidUserWithAnExistingLocation() throws
            EntityNotFoundException {
        // Arrange
        when(IUserProxy.getUser(any()))
                .thenReturn(new UserBean(UUID.randomUUID(), "janedoe", "4105551212", "42 Main St"));
        doNothing().when(IRewardProxy).calculateRewards(any());
        UUID userId = UUID.randomUUID();
        LocationBean locationBean = new LocationBean(10.0, 10.0);
        Location location = new Location(10.0, 10.0);

        VisitedLocationBean visitedLocationBean = new VisitedLocationBean(userId, locationBean, new Date(1L));
        VisitedLocation visitedLocation = new VisitedLocation(userId, location, new Date(1L));

        when(gpsUtil.getUserLocation(any())).thenReturn(visitedLocation);

        // Act and Assert
        assertEquals(10.0, iGpsService.trackUserLocation(UUID.randomUUID()).location.latitude);
        verify(IUserProxy).getUser(any());
        verify(IRewardProxy).calculateRewards(any());
        verify(gpsUtil).getUserLocation(any());
    }

    @Test
    void testGetDistance_shouldReturnTheDistanceBetweenTwoLocations_forTwoValidLocations() {
        LocationBean location = new LocationBean(12.343543, 34.234235);
        LocationBean locationBean2 = new LocationBean(12.343543, 34.234235);

        assertEquals(0, iGpsService.getDistance(location, locationBean2), 0);
    }

    @Test
    void testAddShutDownHook() {
        // Arrange
        GpsService gpsService = new GpsService();

        // Act
        gpsService.addShutDownHook();

        // Assert nothing has changed
        assertEquals(Integer.MAX_VALUE, gpsService.getAttractionProximityRange());
        assertTrue(gpsService.iTracker instanceof Tracker);
        assertEquals(0, gpsService.getProximityBuffer());
    }

    @Test
    void testGetAttractionProximityRange_shouldReturn100() {
        GpsService gpsService = new GpsService();
        int attractionProximityRange = gpsService.getAttractionProximityRange();
        assertEquals(Integer.MAX_VALUE, attractionProximityRange);
    }

    @Test
    void testSetAttractionProximityRange_shouldSetTheAttractionProximityRangeTo388() {
        iGpsService.setAttractionProximityRange(388);
        int attractionProximityRange = iGpsService.getAttractionProximityRange();
        assertEquals(388, attractionProximityRange);
    }

    @Test
    void testIsWithinAttractionProximity_shouldReturnTrue_forTwoAttractionWithinDefinedTheProximityRange() {

        Location location = new Location(12.343543, 34.234235);
        LocationBean locationBean = new LocationBean(location.latitude,
                location.longitude);
        LocationBean locationBean2 = new LocationBean(12.343543, 34.234235);

        assertTrue(iGpsService.isWithinAttractionProximity(locationBean, locationBean2));
    }

    @Test
    void testSetProximityBuffer_shouldSetTheProximityBufferTo309() {
        iGpsService.setProximityBuffer(309);
        assertEquals(309, iGpsService.getProximityBuffer());
    }

    @Test
    void testTrackUserLocation_shouldThrowAEntityNotFoundException_forANonExistingUserId() throws
            EntityNotFoundException {
        // Arrange
        when(this.IUserProxy.getUser(any())).thenReturn(null);
        doNothing().when(this.IRewardProxy).calculateRewards(any());
        UUID userId = UUID.randomUUID();
        Location location = new Location(10.0, 10.0);
        when(this.gpsUtil.getUserLocation(any()))
                .thenReturn(new VisitedLocation(userId, location, new Date(1L)));
        // Act and Assert
        EntityNotFoundException entityNotFoundException = assertThrows(EntityNotFoundException.class,
                () -> this.iGpsService.trackUserLocation(userId));
        assertEquals("User with id " + userId + " could not be found", entityNotFoundException.getMessage());
        verify(this.IUserProxy).getUser(any());
    }

    @Test
    void testTrackUserLocationAsync_shouldReturnAVisitedLocationForAValidUser() {
        // Arrange
        UserBean fakeUser = new UserBean(UUID.randomUUID(), "type", "42", "42 Main St");
        when(IUserProxy.getUser(fakeUser.getUserId())).thenReturn(fakeUser);
        UUID userId = UUID.randomUUID();
        Location location = new Location(10.0, 10.0);
        VisitedLocation visitedLocation = new VisitedLocation(userId, location, new Date(1L));
        when(gpsUtil.getUserLocation(any())).thenReturn(visitedLocation);
        // Act and Assert
        assertNotNull(
                iGpsService.trackUserLocationAsync(new ArrayList<>(
                        Collections.singleton(fakeUser))));
    }

}