package com.opc.gps.service;

import com.opc.gps.bean.UserBean;
import com.opc.gps.exception.EntityNotFoundException;
import com.opc.gps.proxy.IUserProxy;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.time.StopWatch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@Service
@Slf4j
public class Tracker extends Thread implements ITracker {

    private static final long TRACKING_POLLING_INTERVAL = TimeUnit.MINUTES.toSeconds(5);

    private final ExecutorService executorService = Executors.newSingleThreadExecutor();

    private boolean stop = false;

    @Autowired
    IUserProxy iUserProxy;

    @Autowired
    IGpsService iGpsService;

    public Tracker() {
        executorService.submit(this);
    }

    /**
     * Assures to shut down the Tracker thread
     */
    @Override public void stopTracking() {
        stop = true;
        executorService.shutdownNow();
    }

    /**
     * Tracks users location asynchronously at start up. Keeps running in the background unless explicitly stopped.
     */
    @Override
    public void run() {
        StopWatch stopWatch = new StopWatch();
        while (true) {
            if (interruptEventReceived()) {
                log.debug("Tracker stopping");
                break;
            }

            List<UserBean> userBeans = new ArrayList<>(iUserProxy.getAllUsers().values());
            CopyOnWriteArrayList<UserBean> usersCopy = new CopyOnWriteArrayList<>(userBeans);

            logTime("Begin Tracker. Tracking {0} users.", userBeans.size());
            stopWatch.start();
            if (!usersCopy.isEmpty()) {
                trackUsersLocations(usersCopy);
            }
            stopWatch.stop();
            logTime("Tracker Time Elapsed: {0} seconds.", TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()));
            stopWatch.reset();

            // introduce latency to simulate processing time
            try {
                log.debug("Tracker sleeping");
                TimeUnit.SECONDS.sleep(TRACKING_POLLING_INTERVAL);
            }
            catch (InterruptedException e) {
                log.error(MessageFormat.format("interrupted: {0}", e.getMessage()));
                stopCurrentThread();
            }
        }

    }

    private boolean interruptEventReceived() {
        return Thread.currentThread().isInterrupted() || stop;
    }

    private void trackUsersLocations(CopyOnWriteArrayList<UserBean> usersCopy) {
        usersCopy.forEach(user -> {
            try {
                iGpsService.trackUserLocation(user.getUserId());
            }
            catch (EntityNotFoundException e) {
                log.error(MessageFormat.format("Entity not found: {0}", e.getMessage()));
                stopCurrentThread();
            }
        });
    }

    private void logTime(String s, long l) {
        if (log.isDebugEnabled()) {
            log.debug(MessageFormat.format(s,
                    l));
        }
    }

    private void stopCurrentThread() {
        Thread.currentThread().interrupt();
    }
}