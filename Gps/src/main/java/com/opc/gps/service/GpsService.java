package com.opc.gps.service;

import com.opc.gps.bean.LocationBean;
import com.opc.gps.bean.UserBean;
import com.opc.gps.bean.VisitedLocationBean;
import com.opc.gps.exception.EntityNotFoundException;
import com.opc.gps.model.NearByAttraction;
import com.opc.gps.proxy.IRewardProxy;
import com.opc.gps.proxy.IUserProxy;
import gpsUtil.GpsUtil;
import gpsUtil.location.Attraction;
import gpsUtil.location.VisitedLocation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ForkJoinPool;
import java.util.stream.Collectors;

import static org.reflections.Reflections.log;

@Service
@Slf4j
public class GpsService implements IGpsService {

    private static final double STATUTE_MILES_PER_NAUTICAL_MILE = 1.15077945D;

    @Value("${attraction.proximity.buffer}")
    private int proximityBuffer;

    private int attractionProximityRange = Integer.MAX_VALUE;

    @Autowired
    GpsUtil gpsUtil;

    @Autowired
    ITracker iTracker;

    @Autowired
    IRewardProxy iRewardProxy;

    @Autowired
    IUserProxy iUserProxy;

    private static ForkJoinPool forkJoinPool = new ForkJoinPool(2);

    public GpsService() {
        this.iTracker = new Tracker();
        Locale.setDefault(Locale.US);
        addShutDownHook();
    }

    /**
     * Gets the attraction proximity range.
     *
     * @return the attraction proximity range
     */
    @Override public int getAttractionProximityRange() {
        return attractionProximityRange;
    }

    /**
     * Sets the attraction proximity range.
     *
     * @param attractionProximityRange the attraction proximity range
     * @return the attraction proximity range
     */
    @Override public int setAttractionProximityRange(int attractionProximityRange) {
        this.attractionProximityRange = attractionProximityRange;
        return this.attractionProximityRange;
    }

    /**
     * Gets a user's location.
     *
     * @param userId the user id
     * @return the user location
     * @throws EntityNotFoundException if no user could be found using the provided id.
     */
    @Override public VisitedLocationBean getUserLocation(UUID userId) throws EntityNotFoundException {
        UserBean user = findUser(userId);
        return (user.getVisitedLocations().isEmpty()) ?
                trackUserLocation(userId) :
                user.getLastVisitedLocation();
    }

    /**
     * Checks if an attraction is within proximity range.
     *
     * @param attraction the attraction
     * @param location   the location
     * @return a boolean, true if the attraction is within proximity range, false otherwise
     */
    @Override public boolean isWithinAttractionProximity(LocationBean attraction, LocationBean location) {
        log.debug("received isWithinAttractionProximity request");
        return (getDistance(attraction, location) < attractionProximityRange);
    }

    /**
     * Sets the proximity buffer.
     *
     * @param proximityBuffer the proximity buffer
     * @return the proximity buffer
     */
    @Override public int setProximityBuffer(int proximityBuffer) {
        this.proximityBuffer = proximityBuffer;
        return this.getProximityBuffer();
    }

    /**
     * Track user's visited location and starts reward calculation process asynchronously.
     *
     * @param userId the user
     * @return the visited location
     * @throws EntityNotFoundException if no user could be found using the provided id.
     */
    @Override public VisitedLocationBean trackUserLocation(UUID userId) throws EntityNotFoundException {
        UserBean user = findUser(userId);
        VisitedLocation userLocation = gpsUtil.getUserLocation(userId);
        VisitedLocationBean visitedLocation = new VisitedLocationBean(userId,
                new LocationBean(userLocation.location.latitude, userLocation.location.longitude)
                , userLocation.timeVisited);
        user.addToVisitedLocations(visitedLocation);
        iRewardProxy.calculateRewards(userId);
        return visitedLocation;
    }

    /**
     * Track user location asynchronously.
     *
     * @param userBeanList the user
     * @return the visited location
     */
    @Override public List<VisitedLocationBean> trackUserLocationAsync(List<UserBean> userBeanList) {

        CustomRecursiveTask customRecursiveTask = new CustomRecursiveTask(userBeanList);
        forkJoinPool.execute(customRecursiveTask);
        List<VisitedLocationBean> visitedLocationBeans = customRecursiveTask.join();
        CompletableFuture.runAsync(() -> {
                    try {
                        iRewardProxy.calculateRewardsAsync(userBeanList);
                    }
                    catch (Exception e) {
                        log.error("error when calculating user location", e);
                    }
                }
        );
        return visitedLocationBeans;
    }

    /**
     * Gets the proximity buffer used to evaluate if an attraction is nearBy.
     *
     * @return the proximity buffer
     */
    @Override public int getProximityBuffer() {
        return proximityBuffer;
    }

    /**
     * Gets nearby attractions for a visited Location.
     *
     * @param visitedLocation the visited location
     * @return the nearby attractions
     */
    @Override public List<NearByAttraction> getNearByAttractions(VisitedLocationBean visitedLocation) {
        List<NearByAttraction> nearbyAttractions = new ArrayList<>();
        setAttractionProximityRange(Integer.MAX_VALUE);
        for (Attraction attraction : gpsUtil.getAttractions()) {
            LocationBean attractionLocation = new LocationBean(attraction.latitude, attraction.longitude);

            if (isWithinAttractionProximity(attractionLocation, visitedLocation.location)) {
                NearByAttraction singleAttraction = initNearByAttraction(visitedLocation, attraction);
                nearbyAttractions.add(singleAttraction);
            }
        }

        return nearbyAttractions.stream()
                                .sorted(Comparator.comparing(NearByAttraction::getDistanceInMiles))
                                .limit(5)
                                .collect(
                                        Collectors.toList());
    }

    /**
     * Gets the distance between 2 locations.
     *
     * @param loc1 the first location
     * @param loc2 the second location
     * @return the distance
     */
    @Override public double getDistance(LocationBean loc1, LocationBean loc2) {
        double lat1 = Math.toRadians(loc1.latitude);
        double lon1 = Math.toRadians(loc1.longitude);
        double lat2 = Math.toRadians(loc2.latitude);
        double lon2 = Math.toRadians(loc2.longitude);
        double angle = Math.acos(
                Math.sin(lat1) * Math.sin(lat2) + Math.cos(lat1) * Math.cos(lat2) * Math.cos(lon1 - lon2));
        double nauticalMiles = 60.0D * Math.toDegrees(angle);
        return STATUTE_MILES_PER_NAUTICAL_MILE * nauticalMiles;
    }

    /**
     * Gently shuts down the running process.
     */
    @Override public void addShutDownHook() {
        Runtime.getRuntime().addShutdownHook(new Thread(iTracker::stopTracking));
    }

    /**
     * Track user location asynchronously for a specific user count.
     *
     * @param usersCount the users count
     * @return the list
     */
    @Override public List<VisitedLocationBean> trackUserLocationAsyncForACount(int usersCount) {
        iUserProxy.setInternalUserNumber(usersCount);
        Map<String, UserBean> allUsers = iUserProxy.getAllUsers();
        List<UserBean> generatedUsersList = new ArrayList<>(allUsers.values());
        return trackUserLocationAsync(generatedUsersList);

    }

    /**
     * Initialize a NearByAttraction Object form a VisitedLocation and an Attraction
     *
     * @param visitedLocation the visited location
     * @param attraction      the attraction
     * @return the initialized NearByAttraction
     */
    private NearByAttraction initNearByAttraction(VisitedLocationBean visitedLocation, Attraction attraction) {
        NearByAttraction singleAttraction = new NearByAttraction();
        singleAttraction.setName(attraction.attractionName);
        LocationBean attractionLocation = new LocationBean(attraction.latitude, attraction.longitude);
        singleAttraction.setAttractionLocation(attractionLocation);
        LocationBean userLocation = new LocationBean(visitedLocation.location.latitude,
                visitedLocation.location.longitude);
        singleAttraction.setUserLocation(userLocation);

        singleAttraction.setDistanceInMiles(getDistance(attractionLocation, userLocation));
        singleAttraction.setRewardPoints(
                iRewardProxy.getAttractionRewardPoints(attraction.attractionId, visitedLocation.userId));
        return singleAttraction;
    }

    /**
     * Find a user by its id
     *
     * @param userId the user id
     * @return the found user
     * @throws EntityNotFoundException if no user could be found using the provided id.
     */
    private UserBean findUser(UUID userId) throws EntityNotFoundException {
        UserBean user = iUserProxy.getUser(userId);
        if (user == null) {
            throw new EntityNotFoundException("User with id " + userId + " could not be found");
        }
        return user;
    }


}