package com.opc.gps.controller;

import com.opc.gps.bean.LocationBean;
import com.opc.gps.bean.VisitedLocationBean;
import com.opc.gps.exception.EntityNotFoundException;
import com.opc.gps.model.NearByAttraction;
import com.opc.gps.service.IGpsService;
import com.opc.gps.validator.Uuid;
import gpsUtil.location.Location;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.constraints.Range;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Positive;
import java.text.MessageFormat;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@RestController
@Slf4j
@Api(value = "Tracker methods to start and stop tracker or get a user's location.")
@RequestMapping("/gps")
public class GpsController {

    @Autowired
    IGpsService iGpsService;

    /**
     * Track user location visited location.
     *
     * @param userId the user bean
     * @return the visited location
     * @throws EntityNotFoundException if the user is not found
     */
    @ApiOperation(value = "Gets a user's location by user id returning a VisitedLocation entity and synchronously " +
                          "starts reward " +
                          "calculation process")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "The visited location"),
                    @ApiResponse(code = 400, message = "If the id is not a valid uuid")
            }
    )
    @PostMapping("/track/{userId}")
    public VisitedLocationBean trackUserLocation(@PathVariable("userId") UUID userId) throws
            EntityNotFoundException {
        return iGpsService.trackUserLocation(userId);
    }

    /**
     * Sets the attraction proximity range.
     *
     * @param range the range
     * @return the new attraction proximity range value
     */
    @ApiOperation(value = "Sets the attraction proximity range.")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "The new attraction proximity range"),
                    @ApiResponse(code = 400, message = "In case the the provided range is not a valid integer or is " +
                                                       "lower than 1.")
            }
    )
    @PostMapping("/proximityRange")
    public int setAttractionProximityRange(@RequestBody @Positive int range) {
        return iGpsService.setAttractionProximityRange(range);
    }

    /**
     * Gets the attraction proximity range.
     *
     * @return the attraction proximity range value
     */
    @ApiOperation(
            value = "Gets the attraction proximity range.")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "The attraction proximity range"),
            }
    )
    @GetMapping("/proximityRange")
    public int getAttractionProximityRange() {
        return iGpsService.getAttractionProximityRange();
    }

    /**
     * Sets the proximity buffer.
     *
     * @param buffer the buffer
     * @return the proximity buffer
     */
    @ApiOperation(
            value = "Sets the proximity buffer.")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "empty response"),
                    @ApiResponse(code = 400, message = "In case the provided buffer is lower than 0")
            }
    )
    @PostMapping("/proximityBuffer")
    public int setProximityBuffer(@RequestBody @Positive int buffer) {
        return iGpsService.setProximityBuffer(buffer);
    }

    /**
     * Gets the distance between two locations.
     *
     * @param body the request body must be a Map containing the attraction and location to be compared
     * @return the distance
     * @see Location
     * @see LocationBean
     */
    @ApiOperation(
            value = "Gets the distance between two locations.")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "The distance between the provided location"),
            }
    )
    @PostMapping("/distance")
    public double getDistance(@RequestBody Map<String, LocationBean> body) {
        LocationBean attraction = body.get("attraction");
        LocationBean location = body.get("location");
        return iGpsService.getDistance(attraction, location);
    }

    /**
     * Verifies if a location is within attraction proximity range.
     *
     * @param body the request body containing the attraction and location to be compared
     * @return a boolean, true if the attraction is within the proximity range, false otherwise.
     */
    @ApiOperation(
            value = "Verifies if a location is within attraction proximity range.")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "True if the location is within the attraction's proximity " +
                                                       "range, false otherwise"),
            }
    )
    @PostMapping("/isWithinAttractionProximity")
    public boolean isWithinAttractionProximity(@RequestBody Map<String, LocationBean> body) {
        LocationBean attraction = body.get("attraction");
        LocationBean location = body.get("location");
        return iGpsService.isWithinAttractionProximity(attraction, location);
    }

    /**
     * Track user location asynchronously.
     *
     * @param usersCount the user bean
     * @return the visited location
     */
    @ApiOperation(value = "Gets a user's location by user id returning a VisitedLocation entity and asynchronously " +
                          "starts reward " +
                          "calculation process")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "The visited location"),
                    @ApiResponse(code = 400, message = "If the id is not a valid uuid")

            }
    )
    @GetMapping("/track/async/{usersCount}")
    public List<VisitedLocationBean> trackUserLocationAsync(
            @PathVariable("usersCount") @Range(min = 1, max = 100_000) int usersCount) {
        log.info(MessageFormat.format("userCount: {0} ", usersCount));
        return iGpsService.trackUserLocationAsyncForACount(usersCount);
    }

    /**
     * Creates a shutdown hook to gently shut down the running tracking thread.
     */
    @ApiOperation(value = "Gently interrupts the running tracking process.")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "Empty response"),
            }
    )
    @PostMapping("/track/stop")
    public void stopTracking() {
        log.info("received stop tracking request.");
        iGpsService.addShutDownHook();
        log.info("added shutdown hook.");
    }

    /**
     * Gets the users visited location. If the user has no visited locations then a tracking process is initiated,
     * then the found location is returned.
     *
     * @param userId the user id
     * @return VisitedLocation a visited location
     * @throws EntityNotFoundException if the user is not found
     */
    @ApiOperation(
            value = "Gets the users visited location. If the user has no visited locations then a tracking process is" +
                    " initiated, then the found location is returned.", notes = "${notes.entityNotFoundExceptionNote}")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "The visited location"),
                    @ApiResponse(code = 400, message = "If the id is not a valid uuid")
            }
    )
    @Validated
    @GetMapping("/locations/{userId}")
    public VisitedLocationBean getUserLocation(@PathVariable("userId") @Uuid UUID userId) throws
            EntityNotFoundException {
        return iGpsService.getUserLocation(userId);
    }

    /**
     * Gets nearby attractions.
     *
     * @param visitedLocation the visited location
     * @return the nearby attractions
     */
    @ApiOperation(value = "Gets nearby attractions")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "A list of the 5 most nearby Attractions or an empty list if " +
                                                       "no nearby attractions were found"),
                    @ApiResponse(code = 400, message = "If the provided visited location is invalid")
            }
    )
    @PostMapping("/nearByAttractions")
    public List<NearByAttraction> getNearByAttractions(@RequestBody VisitedLocationBean visitedLocation) {
        return iGpsService.getNearByAttractions(visitedLocation);
    }
}