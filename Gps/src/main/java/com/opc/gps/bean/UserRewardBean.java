package com.opc.gps.bean;

import gpsUtil.location.Attraction;
import gpsUtil.location.VisitedLocation;

public class UserRewardBean {

    /**
     * The Visited location.
     */
    public final gpsUtil.location.VisitedLocation VisitedLocation;

    /**
     * The Attraction.
     */
    public final Attraction attraction;

    private int rewardPoints;

    /**
     * Instantiates a new User reward bean.
     *
     * @param VisitedLocation the visited location
     * @param attraction      the attraction
     * @param rewardPoints    the reward points
     */
    public UserRewardBean(VisitedLocation VisitedLocation, Attraction attraction, int rewardPoints) {
        this.VisitedLocation = VisitedLocation;
        this.attraction      = attraction;
        this.rewardPoints    = rewardPoints;
    }

    /**
     * Instantiates a new User reward bean.
     *
     * @param VisitedLocation the visited location
     * @param attraction      the attraction
     */
    public UserRewardBean(VisitedLocation VisitedLocation, Attraction attraction) {
        this.VisitedLocation = VisitedLocation;
        this.attraction      = attraction;
    }

    /**
     * Sets reward points.
     *
     * @param rewardPoints the reward points
     */
    public void setRewardPoints(int rewardPoints) {
        this.rewardPoints = rewardPoints;
    }

    /**
     * Gets reward points.
     *
     * @return the reward points
     */
    public int getRewardPoints() {
        return this.rewardPoints;
    }
}