package com.opc.gps.model;

import com.opc.gps.bean.LocationBean;

public class NearByAttraction {

    private String name;

    private LocationBean attractionLocation;

    private LocationBean userLocation;

    private double distanceInMiles;

    private int rewardPoints;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocationBean getAttractionLocation() {
        return attractionLocation;
    }

    public void setAttractionLocation(LocationBean attractionLocation) {
        this.attractionLocation = attractionLocation;
    }

    public LocationBean getUserLocation() {
        return userLocation;
    }

    public void setUserLocation(LocationBean userLocation) {
        this.userLocation = userLocation;
    }

    public double getDistanceInMiles() {
        return distanceInMiles;
    }

    public void setDistanceInMiles(double distanceInMiles) {
        this.distanceInMiles = distanceInMiles;
    }

    public int getRewardPoints() {
        return rewardPoints;
    }

    public void setRewardPoints(int rewardPoints) {
        this.rewardPoints = rewardPoints;
    }
}